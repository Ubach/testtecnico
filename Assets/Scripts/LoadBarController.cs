﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class LoadBarController : MonoBehaviour
{
    public Slider progressBar;
    // Start is called before the first frame update
    void Start()
    {
        LoadBar();
    }

    public void LoadBar()
    {
        progressBar.DOValue(1, 5).SetEase(Ease.InOutSine).OnComplete(() => LoadScene());
    }

    public void LoadScene()
    {
        SceneManager.LoadScene("MainScreen");
    }
}
