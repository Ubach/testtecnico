﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleController : MonoBehaviour
{
    public TextMeshProUGUI title;
    // Start is called before the first frame update
    void Start()
    {
        title.text = SceneManager.GetActiveScene().name;
    }
}
