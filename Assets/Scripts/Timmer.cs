﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

public class Timmer : MonoBehaviour
{

    [Serializable]
    public class TimeApi
    {
        public string abbreviation = "";
        public string client_ip = "";
        public string datetime = "";
        public string day_of_week = "";
        public string day_of_year = "";
        public string dst = "";
        public string dst_from = "";
        public string dst_offset = "";
        public string dst_until = "";
        public string raw_offset = "";
        public string timezone = "";
        public string unixtime = "";
        public string utc_datetime = "";
        public string utc_offset = "";
        public string week_number = "";
    }

    public TextMeshProUGUI timeTMP;

    // Start is called before the first frame update
    void OnEnable()
    {
        StartCoroutine(CallApi()); 
    }

    public IEnumerator CallApi() {

        UnityWebRequest www = UnityWebRequest.Get("http://worldtimeapi.org/api/ip");

        //yield return new WaitUntil(()=> www.isDone);

        yield return www.SendWebRequest();

        if (www.isNetworkError) {
            Debug.Log(www.error);
            timeTMP.text = "ERROR receiving info from api";
        }
        else
        {
            TimeApi time = JsonUtility.FromJson<TimeApi>(www.downloadHandler.text);

            string[] splitedTime = time.datetime.Split('T');
            string[] splitedTime2 = splitedTime[1].Split('.');

            timeTMP.text = splitedTime[0] + " " + splitedTime2[0];
        }

        yield return new WaitForEndOfFrame();
    }
}
